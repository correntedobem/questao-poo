package gestaoestufas;

/**
 *
 * @author Elvis Souza Dias
 */
public abstract class Estufa {
	private String codigo;
	private double area;
	private double valor;
	private double tempMin;
	private double tempMax;
	private double tempIdeal;
	private boolean situacao;
	
	private String tipo;
	private DadoClimatico dadosClimaticos[] = new DadoClimatico[100];
	private int indiceClimatico = 0;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public abstract double getValorComercial();

	public boolean getSituacao() {
		return situacao;
	}

	public void setSituacao(boolean situacao) {
		this.situacao = situacao;
	}
	int contFr = 1;
		int contFl = 1;
		int contLe = 1;

	public void setCodigo(String tipo) {
		
		if (tipo.equalsIgnoreCase("fruta")) {
			String contF = String.valueOf(contFr);
			while (contF.length() < 3) {
				contF = "0" + contF;
			}
			contFr = contFr +1;
			this.codigo = "FR - " + contF;
		} else if (tipo.equalsIgnoreCase("flor")) {
			String contF1 = String.valueOf(contFl);
			while (contF1.length() < 3) {
				contF1 = "0" + contF1;
			}
			contFl++;
			this.codigo = "FL - " + contF1;
		} else if (tipo.equalsIgnoreCase("legumes")) {
			String contL = String.valueOf(contLe);
			while (contL.length() < 3) {
				contL = "0" + contL;
			}
			this.codigo = "LM - " + contLe;
			
		}
	}

	public String getCodigo() {
		return codigo;
	}

	public double getArea() {
		return area;
	}

	protected void setArea(double area) {
		this.area = area;
	}

	public double getValor() {
		return valor;
	}

	protected void setValor(double valor) {
		this.valor = valor;
	}

	public double getTempMin() {
		return tempMin;
	}

	protected void setTempMin(double tempMin) {
		this.tempMin = tempMin;
	}

	public double getTempMax() {
		return tempMax;
	}

	protected void setTempMax(double tempMax) {
		this.tempMax = tempMax;
	}

	public double getTempIdeal() {
		return tempIdeal;
	}

	protected void setTempIdeal(double tempIdeal) {
		this.tempIdeal = tempIdeal;
	}

	public void adicionarDadoClimatico(String codigo, Tempo horario, double tempColeta) {
		dadosClimaticos[indiceClimatico] = new DadoClimatico(codigo, horario, tempColeta);
		indiceClimatico++;
		
		if(tempColeta > tempMax || tempColeta < tempMin){
			System.out.println("Estufa em risco");
		}
	}

	

}