package gestaoestufas;

import java.io.Serializable;

/**
 *
 * @author Elvis Souza Dias
 */
public class EstufaFruta extends Estufa implements Serializable {
	private double valorComercial;
	private double valorComercialTotal;
	private int unidadesEsperadas;
	private double valorUnit;

	// O construtor recebe os valores para preencher o objeto
	public EstufaFruta(double area, double valor, double tempMin, double tempMax, double tempIdeal
			, int unidadesEsperadas,double valorUnit, String tipo) {
		if (tempMin < tempMax) { // A temperatura mínima não pode ser maior
									// que a máxima e a máximo menor que a
									// mínima
			this.setCodigo(tipo);
			this.setTipo(tipo);
			this.setArea(area);
			this.setValor(valor);
			this.setTempMin(tempMin);
			this.setTempMax(tempMax);
			this.valorUnit = valorUnit;
			this.setTempIdeal(tempIdeal);
			this.setSituacao(true); // Assim que uma estufa for criada ela já
									// virá ATIVA
		} else {
			System.out.println("Temperatura inválida!");
		} /*
			 * O objeto só é instanciado se todas essas condições forem
			 * atendidas
			 */
	}

	public void setValorComercial(double valorComercial) {
		this.valorComercial = this.unidadesEsperadas * this.getValor();
		// Produto do preço unitário (valor fixo) pelo numero de unidades que
		// se espera produzir.
	}

	@Override
	public double getValorComercial() {
		return this.valorComercial;
	}

	public void setValorComercialTotal(double valorComercialTotal) {
		// O valor comercial total atual é a soma do valor fixo com o seu valor
		// comercial.
		this.valorComercialTotal = this.getValor() + this.getValorComercial();
	}

	public double getValorComercialTotal() {
		return this.valorComercialTotal;
	}

}