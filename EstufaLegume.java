package gestaoestufas;

import java.io.Serializable;

public class EstufaLegume extends Estufa implements Serializable {
	private double totalKgEsperado;
	private double precoKg;
	private double umidadeAtual;

	public double getTotalKgEsperado() {
		return totalKgEsperado;
	}

	private void setTotalKgEsperado(double totalKgEsperado) {
		this.totalKgEsperado = totalKgEsperado;
	}

	public double getPrecoKg() {
		return precoKg;
	}

	private void setPrecoKg(double precoKg) {
		this.precoKg = precoKg;
	}

	public double getUmidadeAtual() {
		return umidadeAtual;
	}

	private void setUmidadeAtual(double umidadeAtual) {
		this.umidadeAtual = umidadeAtual;
	}

	// Lista de parâmetros, diferentes de EstufaFlor e EstufaFruta
	private double valorComercial;
	// Cálculo do valorComercial será diferente dos outros, nesse classe.

	public EstufaLegume(double area, double valor, double tempMin, double tempMax, double tempIdeal,
			double totalKgEsperado, double precoKg, String tipo) {
		if (tempMin > tempMax) {

		} else {
			this.setPrecoKg(precoKg);
			this.setTipo(tipo);
			this.setTotalKgEsperado(totalKgEsperado);
			this.setArea(area);
			this.setCodigo(tipo);
			this.contLe = contLe + 1;
			this.setTempMin(tempMin);
			this.setTempMax(tempMax);
			this.setTempIdeal(tempIdeal);
			this.setValor(valor);
			this.setSituacao(true); /*
									 * Alterei o modo de ativar a estufa legume
									 * (Agora ela é ativada automaticamente e
									 * não com parâmetro)
									 */
		}

	}

	@Override
	public double getValorComercial() {
		return this.valorComercial;
	}

}