package gestaoestufas;

/**
 * Created by Chinildo on 04/05/2017.
 */

public class DadoClimatico extends GestaoEstufas {
	private Tempo horario;
	private double temperatura;
	private double umidade;
	private String codigo;

	public Tempo getHorario() {
		return this.horario;
	}

	private void setHorario(Tempo horario) {
		this.horario = horario;
	}

	public double getTemperatura() {
		return temperatura;
	}

	private void setTemp(double temperatura) {
		this.temperatura = temperatura;
	}

	public double getUmidade() {
		return umidade;
	}

	private void setUmidade(double umidade) {
		this.umidade = umidade;
	}

	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		for (int i = 0; i < getlistaEstufa().length; i++) {
			if (codigo.equalsIgnoreCase(getlistaEstufa()[i].getCodigo())) {
				this.codigo = codigo;
				break;
			}
		}
	}

	

	public DadoClimatico(String codigo,Tempo horario, double tempCole) {
		setCodigo(codigo);
		setHorario(horario);
		setTemp(tempCole);
	}
}
