package gestaoestufas;

/**
 * Created by Chinildo on 01/05/2017.
 
 */
public class Tempo {
	private Integer dia = 0;
	private Integer mes = 0;
	private Integer ano = 0;
	private Integer hora = 0;
	private Integer min = 0;
	private Integer seg = 0;

	public int getDia() {
		return dia;
	}

	protected void setDia(int dia) {
		this.dia = dia;
	}

	public int getMes() {
		return mes;
	}

	protected void setMes(int mes) {
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	protected void setAno(int ano) {
		this.ano = ano;
	}

	public int getHora() {
		return hora;
	}

	protected void setHora(int Hr) {
		this.hora = Hr;
	}

	public int getMin() {
		return min;
	}

	protected void setMin(int min) {
		this.min = min;
	}

	public int getSeg() {
		return seg;
	}

	protected void setSeg(int seg) {
		this.seg = seg;
	}

	public Tempo(int dia, int mes, int ano, int hr, int min, int seg) {
		validarData(dia, mes, ano);
		validarHora(hr, min, seg);
	}

	private boolean verificarBissexto(int ano) {
		if (ano % 400 == 0) {
			return true;
		} else if ((ano % 4 == 0) && (ano % 100 != 0)) {
			return true;
		}
		return false;
	}

	private boolean validarData(int Dia, int Mes, int Ano) {
		boolean dia30 = true;
		int dia28 = 28;
		boolean bissexto = verificarBissexto(Ano);
		if (bissexto) {
			dia28 = 29;
		}
		if (Mes == 1 || Mes == 3 || Mes == 5 || Mes == 7 || Mes == 8 || Mes == 10 || Mes == 12) {
			dia30 = false;
		}
		if ((Ano < 0 || Ano > 2017) || (Mes < 1 || Mes > 12) || (Mes == 2 && (Dia < 1 || Dia > dia28))
				|| (dia30 && (Dia < 1 || Dia > 30))) {
			System.out.println("Data Inv�lida");
			return false;
		} else {
			this.ano = Ano;
			this.dia = Dia;
			this.mes = Mes;
			return true;
		}
	}

	private boolean validarHora(int Hr, int Mn, int Se) {
		if ((Hr < 0 || Hr >= 24) || (Mn < 0 || Mn >= 60) || (Se < 0 || Se >= 60)) {
			System.out.println("Hora Inv�lida");
			return false;
		} else {
			this.hora = Hr;
			this.min = Mn;
			this.seg = Se;
			return true;
		}

	}

	public String toStringData() {
		String ano, mes, dia;
		ano = String.valueOf(getAno());
		mes = String.valueOf(getMes());
		dia = String.valueOf(getDia());
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		while (ano.length() < 4) {
			int cont = ano.length();
			ano = "0" + ano;
		}
		return dia + "/" + mes + "/" + ano;
	}

	public String toStringHora() {
		String hora, min, seg;
		hora = String.valueOf(getHora());
		min = String.valueOf(getMin());
		seg = String.valueOf(getSeg());
		if (hora.length() < 2) {
			hora = "0" + hora;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		return hora + ":" + min + ":" + seg;
	}

}
