package gestaoestufas;

import java.util.*;
import java.io.*;
import java.util.Vector;

/**
 * Created by Chinildo on 06/05/2017.
 */
public class GestaoEstufas {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		GestaoEstufas g1 = new GestaoEstufas();
		g1.cadastrarEstufa("fruta", 34, 5000, 22.5, 30.2, 25.4);
		g1.cadastrarEstufa("legumes",123 , 2000, 10.2, 20, 14);
		g1.cadastrarEstufa("legumes",123 , 2000, 10.2, 20, 14);
		g1.cadastrarEstufa("legumes",123 , 2000, 10.2, 20, 14);
		
		System.out.println(g1.listaEstufas[0].getCodigo());
		System.out.println(g1.listaEstufas[1].getCodigo());
		System.out.println(g1.listaEstufas[2].getCodigo());
		System.out.println(g1.listaEstufas[3].getCodigo());
	
		
		
			
	}

	private Estufa [] listaEstufas = new Estufa [100];
	/*
	 * Vetor do tipo Estufa com índice 100
	 * "O sistema é capaz de controlar, no máximo, 100 estufas" Ao cadastrar
	 * uma nova estufa, preenchemos o vetor Estufa, nesse caso, o vetor deve ter
	 * 100 posições.
	 */
	private int Indice;

	public Estufa[] getlistaEstufa() {
		return this.listaEstufas;
	}

	public void salvarArquivo() {
		// Serializar o arquivo (Salvar na mesma pasta do projeto)
		String ArquivoEstufa = "ArquivoEstufa.txt";
		try {
			File arquivo = new File(ArquivoEstufa);
			if (!arquivo.exists()) {
				arquivo.createNewFile();
			}

			OutputStream out = new FileOutputStream(arquivo);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
			objectOutputStream.writeObject(this.getlistaEstufa()[0]);
			objectOutputStream.close();
			out.close();
		} catch (Exception ex) {

		}
	}

	private void setIndice(boolean cadastrarEstufa) {
		if (cadastrarEstufa) {
			Indice++;
		}
	}

	private void setIndice2(boolean removerEstufa) {
		if (removerEstufa) {
			Indice--;
		}
	}

	/*
	 * Duas funções acima serve para controlar o indice do vetor da lista de
	 * estufas
	 */

	public boolean cadastrarEstufa(String tipo,double area, double valor, double tempMin, double tempMax, double tempIdeal ) {
		Scanner input = new Scanner(System.in);
		if (tipo.equalsIgnoreCase("Fruta")) {
			System.out.print("Qual a quantidade de frutas esperada? ");
			int qtd = input.nextInt();
			System.out.print("Qual o valor comercial unit�rio? ");
			double vlrUnit = input.nextDouble();
			Estufa F1 = new EstufaFruta(area, valor, tempMin, tempMax, tempIdeal, qtd, vlrUnit , tipo);
			listaEstufas[Indice] = F1;
			setIndice(true);
			return true;
		} else if (tipo.equalsIgnoreCase("Flor")) {
			System.out.print("Qual a quantidade de flores esperada? ");
			int qtd = input.nextInt();
			System.out.print("Qual o valor comercial unit�rio? ");
			double vlrUnit = input.nextDouble();
			Estufa FL1 = new EstufaFlor(area, valor, tempMin, tempMax, tempIdeal, qtd,vlrUnit, tipo);
			listaEstufas[Indice] = FL1;
			setIndice(true);
			return true;
		} else if (tipo.equalsIgnoreCase("Legumes")) {
			System.out.print("Qual a quantidade de kg esperados? ");
			double qtdkg = input.nextDouble();
			System.out.print("qual o valor do kg? ");
			double vlrkg = input.nextDouble();
			Estufa L1 = new EstufaLegume(area, valor, tempMin, tempMax, tempIdeal, qtdkg, vlrkg, tipo);
			listaEstufas[Indice] = L1;
			setIndice(true);
			return true;
		} else {
			return false;
		}
	}
	
	public boolean removerEstufa(String codigo) {
		Vector vec = new Vector();
		for (int i = 0; i <= Indice; i++) {
			if (listaEstufas[i].getCodigo().equalsIgnoreCase(codigo)) {
				listaEstufas.remove[i]; 
				return true;
			}
		}
		return false;
	}

	public void carregarDados() {

	}

	public boolean AtivarDesativarEstufa(String codigo, boolean situacaodesejada) {
		for (int i = 0; i <= Indice; i++) {
			if (listaEstufas[i].getCodigo().equalsIgnoreCase(codigo)) {
				listaEstufas[i].setSituacao(situacaodesejada);
				return true;
			}
		}
		return false;
	}

	

	public double obterValorTotalDasEstufas() {
		return 0;
	}

	public void EstufasEmRisco() {
		int j = 1;
		System.out.println("Sequencial	"+"C�digo	"+"Tipo Estufa	"+"Valor	"+"Dia da coleta	"+"Temp min		"+ "Temp max	"+"Temp coletada	");
		for (int i = 0; i < listaEstufas.length; i++) {
			if(listaEstufas[i].getTempMin()>                              || listaEstufas[i].getTempMax()<       ){
				System.out.println(j+"	"+listaEstufas[i].getCodigo()+"	"+listaEstufas[i].getTipo()+"	"+listaEstufas[i].getValor()+"	"+					+ listaEstufas[i].getTempMin()+"	"+listaEstufas[i].getTempMax()+"	"+							);
				j++;
			}
		}
		
	}

	public Estufa EstufasEmRisco(Tempo horario) {
		int j = 1;
		System.out.println("Sequencial	"+"C�digo	"+"Tipo Estufa	"+"Valor	"+"Dia da coleta	"+"Temp min		"+ "Temp max	"+"Temp coletada	");
		for (int i = 0; i < listaEstufas.length; i++) {
			if((listaEstufas[i].getTempMin()>                              || listaEstufas[i].getTempMax()<      ) && horario ==          ){
				System.out.println(j+"	"+listaEstufas[i].getCodigo()+"	"+listaEstufas[i].getTipo()+"	"+listaEstufas[i].getValor()+"	"+					+ listaEstufas[i].getTempMin()+"	"+listaEstufas[i].getTempMax()+"	"+							);
				j++;
			}
		}
	}

	public Estufa EstufasEmRisco(Tempo horario, String tipodeestufa) {
		return listaEstufas[0];
	}

	public double obterTotalPerdas() {
		return 0;
	}

	private void excluirItemVetor(Estufa[] vetor, int indice, int posicaovetor) {
		for (int i = indice; i < posicaovetor; i++) {
				vetor[i] = vetor[i + 1];
		}
	}
	@Override
	public double getValorComercial() {
		return 0;
	}
	/* esse último método é submétodo do excluirEstufa */
}
